/**
 * Server side app.js
 * Express middleware
 */

var express =  require("express"); //take in 3rd party library
var app = express(); //construct express object 

console.log(__dirname); //print out name of directory 
const NODE_PORT = process.env.PORT || 4000; //set port number, terminal: export PORT=4001
console.log(NODE_PORT);

var popQuiz = {
    id: 1,
    question: "How many eggs in the chicken basket?",
    answers: [ {index: 1, desc: "Two"}, {index: 2, desc: "Four"}, {index: 3, desc: "Six"}, {index: 4, desc: "Eight"}],
    remarks: "",
    correct: 1
}

app.get("/api/popquiz", function(req,res){ // /api/popquiz is set by user it can be /popquiz
    console.log("get popquiz");
    res.status(200).json9(popQuiz);
})

app.use(express.static(__dirname + "/../client")); 

//if can't find the end point, please show this heading. 404 page. app.use for initialization or exception cases  
//use no end point 
app.use(function(req,res) {
    //console.log("Nothing found!");
    res.send("<h1> Oops wrong URL</h1>");
})

app.listen(NODE_PORT, function() {
    console.log(`Web App started at ${NODE_PORT}`);
})


