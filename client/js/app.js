/**
 * Client side app.js
 * AngularJS
 */

 "use strict";

 (function() {
     //  ng-app= (angular directives)
     var app = angular.module("PopQuizApp", []); //initialization

     app.controller("PopQuizCtrl", [PopQuizCtrl]); //[] pulls in function PopQuizCtrl()

     // ng-controller=
     function PopQuizCtrl() { //this function is actually a class 
        var self = this;

        self.question = "How many eggs in the chicken basket?";
        self.option1 = "Two";
        self.option2 = "Four";
        self.option3 = "Six";
        self.option4 = "Eight";

     }

 })();